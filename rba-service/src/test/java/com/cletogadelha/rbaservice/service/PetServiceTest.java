package com.cletogadelha.rbaservice.service;

import com.cletogadelha.rbarepository.model.Pet;
import com.cletogadelha.rbarepository.repository.PetRepository;
import com.cletogadelha.rbaservice.service.impl.PetServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PetServiceTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private PetRepository mockedPetRepository;

    @InjectMocks
    private PetServiceImpl petService;

    private List<Pet> listPet;

    @Before
    public void setup() {
        listPet = new ArrayList<>();
        listPet.add(new Pet(1, "Cleto", 1, null));
        listPet.add(new Pet(2, "Cleto2",2, null));
    }

    @Test
    public void shouldSavePet() {
        when(mockedPetRepository.saveAndFlush(any())).thenReturn(listPet.get(0));

        Pet pet = petService.savePet(new Pet());

        assertNotNull(pet);
        assertEquals(pet, listPet.get(0));
    }

    @Test
    public void shouldFindPetById() {
        when(mockedPetRepository.findById(any())).thenReturn(Optional.of(listPet.get(0)));

        Pet pet = petService.findById(1);

        assertNotNull(pet);
        assertEquals(pet, listPet.get(0));
    }

    @Test
    public void shouldThrowExceptionWhenPetNotFound() {
        exception.expect(EntityNotFoundException.class);
        exception.expectMessage("Pet not found!");

        when(mockedPetRepository.findById(any())).thenThrow(new EntityNotFoundException("Pet not found!"));

        petService.findById(1);
    }

}
