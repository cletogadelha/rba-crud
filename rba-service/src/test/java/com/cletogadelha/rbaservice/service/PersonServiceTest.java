package com.cletogadelha.rbaservice.service;

import com.cletogadelha.rbarepository.model.Person;
import com.cletogadelha.rbarepository.model.Pet;
import com.cletogadelha.rbarepository.repository.PersonRepository;
import com.cletogadelha.rbaservice.service.impl.PersonServiceImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private PersonRepository mockedPersonRepository;

    @Mock
    private PetService mockedPetService;

    @Captor
    private ArgumentCaptor<Person> personCaptor;

    @InjectMocks
    private PersonServiceImpl personService;

    private List<Person> listPerson;

    @Before
    public void setup() {
        listPerson = new ArrayList<>();
        listPerson.add(new Person(1, "Cleto", "Oliveira", "Straat 12", new Date(), new HashSet<>()));
        listPerson.add(new Person(2, "Cleto2", "Oliveira2", "Straat 122", new Date(), new HashSet<>()));
    }

    @Test
    public void shouldSavePerson() {
        when(mockedPersonRepository.saveAndFlush(any())).thenReturn(listPerson.get(0));

        Person person = personService.savePerson(new Person());

        assertNotNull(person);
        assertEquals(person, listPerson.get(0));
    }

    @Test
    public void shouldFindPersonById() {
        when(mockedPersonRepository.findById(any())).thenReturn(Optional.of(listPerson.get(0)));

        Person person = personService.findById(1);

        assertNotNull(person);
        assertEquals(person, listPerson.get(0));
    }

    @Test
    public void shouldThrowExceptionWhenPersonNotFound() {
        exception.expect(EntityNotFoundException.class);
        exception.expectMessage("Person not found!");

        when(mockedPersonRepository.findById(any())).thenThrow(new EntityNotFoundException("Person not found!"));

        personService.findById(1);
    }

    @Test
    public void shouldFindUserByFirstNameAndOrLastName() {
        when(mockedPersonRepository.findAll(any(Specification.class))).thenReturn(listPerson);

        Person person = personService.findPersonByFirstNameAndOrLastName(new HashMap<>());

        assertNotNull(person);
        assertEquals(person, listPerson.get(0));
    }

    @Test
    public void shouldThrowExceptionWhenNoListFound() {
        exception.expect(EntityNotFoundException.class);
        exception.expectMessage("No Entity found with the following properties: ");

        when(mockedPersonRepository.findAll(any(Specification.class))).thenReturn(new ArrayList());

        personService.findPersonByFirstNameAndOrLastName(new HashMap<>());
    }

    @Test
    public void shouldLinkPersonToPet() {
        Pet pet = new Pet(1, "Pet", 3, null);
        when(mockedPetService.findById(1)).thenReturn(pet);
        when(mockedPersonRepository.findById(any())).thenReturn(Optional.of(listPerson.get(0)));

        personService.linkPersonToPet(1, 1);
        verify(mockedPersonRepository).saveAndFlush(personCaptor.capture());

        Person capturedPerson = personCaptor.getValue();
        assertFalse(capturedPerson.getPets().isEmpty());
        assertTrue(capturedPerson.getPets().contains(pet));
    }

}
