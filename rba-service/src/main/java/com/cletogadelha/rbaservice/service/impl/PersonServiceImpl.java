package com.cletogadelha.rbaservice.service.impl;

import com.cletogadelha.rbarepository.model.Person;
import com.cletogadelha.rbarepository.model.Pet;
import com.cletogadelha.rbarepository.repository.PersonRepository;
import com.cletogadelha.rbarepository.specification.PersonSpecification;
import com.cletogadelha.rbaservice.service.PersonService;
import com.cletogadelha.rbaservice.service.PetService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
@Transactional
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {

    private final PetService petService;
    private final PersonRepository personRepository;

    @Override
    public Person savePerson(Person person) {
        return personRepository.saveAndFlush(person);
    }

    @Override
    public Iterable<Person> getAllPersons() {
        return personRepository.findAll();
    }

    @Override
    public Person findById(Integer id) {
        return personRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("No Entity found with the following Id: " + id.toString()));
    }

    @Override
    public Person findPersonByFirstNameAndOrLastName(Map<String, String> properties) {
        Specification<Person> whereClause = Specification.where(null);
        List<String> allowedProperties = Arrays.asList("firstName", "lastName");

        for(Map.Entry<String, String> entry : properties.entrySet()) {
            if(allowedProperties.contains(entry.getKey())) {
                whereClause = whereClause.and(PersonSpecification.byColumnNameAndValue(entry.getKey(), entry.getValue()));
            }
        }

        List<Person> allPersons = personRepository.findAll(whereClause);

        if(ObjectUtils.isEmpty(allPersons)) {
            throw new EntityNotFoundException("No Entity found with the following properties: " + properties);
        }

        return allPersons.get(0);
    }

    @Override
    public void linkPersonToPet(Integer personId, Integer petId) {
        Pet petToLink = petService.findById(petId);
        Person personToLink = findById(personId);
        personToLink.linkPet(petToLink);

        personRepository.saveAndFlush(personToLink);
    }

}
