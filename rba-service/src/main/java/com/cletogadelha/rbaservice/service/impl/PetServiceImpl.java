package com.cletogadelha.rbaservice.service.impl;

import com.cletogadelha.rbarepository.model.Pet;
import com.cletogadelha.rbarepository.repository.PetRepository;
import com.cletogadelha.rbaservice.service.PetService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class PetServiceImpl implements PetService {

    private final PetRepository petRepository;

    @Override
    public Pet savePet(Pet pet) {
        return petRepository.saveAndFlush(pet);
    }

    @Override
    public Iterable<Pet> getAllPets() {
        return petRepository.findAll();
    }

    @Override
    public Pet findById(Integer id) {
        return petRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("No Entity found with the following Id: " + id.toString()));
    }

    @Override
    public void deletePet(Integer id) {
        petRepository.deleteById(id);
    }

}
