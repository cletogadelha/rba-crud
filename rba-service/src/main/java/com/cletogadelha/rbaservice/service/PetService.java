package com.cletogadelha.rbaservice.service;

import com.cletogadelha.rbarepository.model.Pet;

public interface PetService {

    Pet savePet(Pet person);

    Iterable<Pet> getAllPets();

    Pet findById(Integer id);

    void deletePet(Integer id);

}
