package com.cletogadelha.rbaservice.service;

import com.cletogadelha.rbarepository.model.Person;

import java.util.Map;

public interface PersonService {

    Person savePerson(Person person);

    Iterable<Person> getAllPersons();

    Person findById(Integer id);

    Person findPersonByFirstNameAndOrLastName(Map<String, String> properties);

    void linkPersonToPet(Integer personId, Integer petId);

}
