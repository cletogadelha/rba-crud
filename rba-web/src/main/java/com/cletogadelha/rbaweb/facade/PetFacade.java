package com.cletogadelha.rbaweb.facade;

import com.cletogadelha.rbarepository.model.Pet;
import com.cletogadelha.rbaservice.service.PetService;
import com.cletogadelha.rbaweb.dto.PetDTO;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
@RequiredArgsConstructor
public class PetFacade {

    private final ModelMapper modelMapper;

    private final PetService petService;

    public Iterable<PetDTO> getAllPets() {
        return convertToDTO(petService.getAllPets());
    }

    public PetDTO savePet(PetDTO pet) {
        return convertToDTO(petService.savePet(convertToEntity(pet)));
    }

    public PetDTO updatePet(PetDTO pet) {
        return convertToDTO(petService.savePet(convertToEntity(pet)));
    }

    public void deletePet(Integer id) {
        petService.deletePet(id);
    }

    private PetDTO convertToDTO(Pet pet) {
        return modelMapper.map(pet, PetDTO.class);
    }

    private Pet convertToEntity(PetDTO pet) {
        return modelMapper.map(pet, Pet.class);
    }

    private Iterable<PetDTO> convertToDTO(Iterable<Pet> petList) {
        return Arrays.asList(modelMapper.map(petList, PetDTO[].class));
    }

}
