package com.cletogadelha.rbaweb.facade;

import com.cletogadelha.rbarepository.model.Person;
import com.cletogadelha.rbaservice.service.PersonService;
import com.cletogadelha.rbaweb.dto.PersonDTO;
import com.cletogadelha.rbaweb.dto.PersonUpdateAddressDTO;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class PersonFacade {

    private final ModelMapper modelMapper;

    private final PersonService personService;

    public Iterable<PersonDTO> getAllPersons() {
        return convertToDTO(personService.getAllPersons());
    }

    public PersonDTO getPersonById(Integer id) {
        return convertToDTO(personService.findById(id));
    }

    public PersonDTO savePerson(PersonDTO person) {
        return convertToDTO(personService.savePerson(convertToEntity(person)));
    }

    public PersonDTO updatePersonAddress(PersonUpdateAddressDTO person, Integer id) {
        Person personById = personService.findById(id);
        personById.setAddress(person.getAddress());

        return convertToDTO(personService.savePerson(personById));
    }

    public PersonDTO findPersonByFirstNameAndOrLastName(Map<String, String> properties) {
        return convertToDTO(personService.findPersonByFirstNameAndOrLastName(properties));
    }

    public void linkPersonToPet(Integer personId, Integer petId) {
        personService.linkPersonToPet(personId, petId);
    }

    private PersonDTO convertToDTO(Person person) {
        return modelMapper.map(person, PersonDTO.class);
    }

    private Person convertToEntity(PersonDTO person) {
        return modelMapper.map(person, Person.class);
    }

    private Iterable<PersonDTO> convertToDTO(Iterable<Person> personList) {
        return Arrays.asList(modelMapper.map(personList, PersonDTO[].class));
    }

}
