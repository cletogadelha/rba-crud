package com.cletogadelha.rbaweb.controller;

import com.cletogadelha.rbaweb.dto.PersonDTO;
import com.cletogadelha.rbaweb.dto.PersonUpdateAddressDTO;
import com.cletogadelha.rbaweb.facade.PersonFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("persons")
@RequiredArgsConstructor
public class PersonController {

    private final PersonFacade personFacade;

    @GetMapping
    public ResponseEntity getAllPersons(@RequestParam Map<String, String> parameters) {
        return ResponseEntity.ok(parameters.size() > 0 ? personFacade.findPersonByFirstNameAndOrLastName(parameters) : personFacade.getAllPersons());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonDTO> findById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(personFacade.getPersonById(id));
    }

    @PostMapping
    public ResponseEntity<PersonDTO> savePerson(@RequestBody @Valid PersonDTO personDTO) {
        return ResponseEntity.ok(personFacade.savePerson(personDTO));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<PersonDTO> updateAdress(@RequestBody @Valid PersonUpdateAddressDTO personUpdateAddressDTO, @PathVariable("id") Integer id) {
        return ResponseEntity.ok(personFacade.updatePersonAddress(personUpdateAddressDTO, id));
    }

    @PutMapping("/{personId}/pets/{petId}")
    public ResponseEntity linkPersonToPet(@PathVariable("personId") Integer personId, @PathVariable("petId") Integer petId) {
        personFacade.linkPersonToPet(personId, petId);
        return ResponseEntity.ok().build();
    }

}
