package com.cletogadelha.rbaweb.controller;

import com.cletogadelha.rbaweb.aspect.ExperimentalEndpoint;
import com.cletogadelha.rbaweb.dto.PetDTO;
import com.cletogadelha.rbaweb.facade.PetFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("pets")
@RequiredArgsConstructor
public class PetController {

    private final PetFacade petFacade;

    @GetMapping
    public ResponseEntity<Iterable<PetDTO>> getAllPets() {
        return ResponseEntity.ok(petFacade.getAllPets());
    }

    @PostMapping
    public ResponseEntity<PetDTO> savePet(@RequestBody @Valid PetDTO petDTO) {
        return ResponseEntity.ok(petFacade.savePet(petDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PetDTO> updatePet(@PathVariable("id") Integer id, @RequestBody @Valid PetDTO petDTO) {
        petDTO.setId(id);
        return ResponseEntity.ok(petFacade.updatePet(petDTO));
    }

    @DeleteMapping("/{id}")
    @ExperimentalEndpoint
    public ResponseEntity<PetDTO> delete(@PathVariable("id") Integer id) {
        petFacade.deletePet(id);
        return ResponseEntity.noContent().build();
    }

}
