package com.cletogadelha.rbaweb.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PetDTO implements Serializable {

    private Integer id;
    @NotBlank
    private String name;
    @NotNull
    private Integer age;

    @JsonBackReference
    private PersonDTO person;

}
