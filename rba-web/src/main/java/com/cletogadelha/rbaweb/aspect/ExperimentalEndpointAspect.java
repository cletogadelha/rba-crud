package com.cletogadelha.rbaweb.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;

@Aspect
@Component
public class ExperimentalEndpointAspect {

    @Value("${enable.experimental.endpoints}")
    private Boolean enableExperimentalEndpoints;

    @Pointcut("execution(@org.springframework.web.bind.annotation.*Mapping * *(..))")
    public void springMappingPointcut() {}

    @Around("springMappingPointcut() && @annotation(com.cletogadelha.rbaweb.aspect.ExperimentalEndpoint)")
    public Object controlExperimentalEndpoints(ProceedingJoinPoint pjp) throws Throwable {
        if(!enableExperimentalEndpoints) {
            HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
            response.sendError(HttpStatus.NOT_FOUND.value());
            return null;
        }
        return pjp.proceed();
    }
}
