package com.cletogadelha.rbaweb;

import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.Assert.assertNotNull;

@SpringBootTest
class RbaApplicationTests {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Test
    void contextLoads() {
        assertNotNull(modelMapper);
        assertNotNull(passwordEncoder);
    }

}
