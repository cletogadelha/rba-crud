package com.cletogadelha.rbaweb.controller;

import com.cletogadelha.rbaweb.dto.PetDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles({"default", "memory"})
@Transactional
public class PetControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private PetDTO pet;

    @Before
    public void setUp() {
        pet = createPet();
    }

    private PetDTO createPet() {
        PetDTO pet = new PetDTO();
        pet.setAge(3);
        pet.setName("Pet");
        return pet;
    }

    @Test
    public void shouldCreatePet() throws Exception {
        mvc.perform(post("/pets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(pet)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.age").value(3))
                .andExpect(jsonPath("$.name").value("Pet"))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    public void shouldUpdatePet() throws Exception {
        MvcResult postResult = mvc.perform(post("/pets")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(pet)))
                                .andExpect(status().isOk())
                                .andExpect(jsonPath("$.id").exists())
                                .andExpect(jsonPath("$.name").value("Pet"))
                                .andReturn();

        PetDTO petCreated = objectMapper.readValue(postResult.getResponse().getContentAsString(), PetDTO.class);

        petCreated.setName("New Name");

        mvc.perform(put("/pets/" + petCreated.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(petCreated)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("New Name"))
                .andExpect(jsonPath("$.id").value(petCreated.getId()));
    }

    @Test
    public void shouldFailWhenExperimentalMethod() throws Exception {
        mvc.perform(delete("/pets/1"))
                .andExpect(status().isNotFound());
    }
}
