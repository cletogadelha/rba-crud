package com.cletogadelha.rbaweb.controller;

import com.cletogadelha.rbaweb.dto.PersonDTO;
import com.cletogadelha.rbaweb.dto.PersonUpdateAddressDTO;
import com.cletogadelha.rbaweb.facade.PersonFacade;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersonControllerTest {

    @Mock
    private PersonFacade mockedPersonFacade;
    @InjectMocks
    private PersonController personController;

    @Mock
    private Map<String, String> mockedParametersMap;

    private List<PersonDTO> listPerson;

    @Before
    public void setup() {
        listPerson = new ArrayList<>();
        listPerson.add(new PersonDTO(1, "Cleto", "Oliveira", "Straat 12", new Date(), new HashSet<>()));
        listPerson.add(new PersonDTO(2, "Cleto2", "Oliveira2", "Straat 122", new Date(), new HashSet<>()));
    }

    @Test
    public void shouldReturnAllPersonsWhenNoParameters() {
        when(mockedPersonFacade.getAllPersons()).thenReturn(listPerson);

        ResponseEntity response = personController.getAllPersons(mockedParametersMap);
        List<PersonDTO> persons = (List<PersonDTO>) response.getBody();

        assertEquals(persons, listPerson);
    }

    @Test
    public void shouldReturnPersonWhenParameters() {
        when(mockedParametersMap.size()).thenReturn(1);
        when(mockedPersonFacade.findPersonByFirstNameAndOrLastName(mockedParametersMap)).thenReturn(listPerson.get(0));

        ResponseEntity response = personController.getAllPersons(mockedParametersMap);
        PersonDTO person = (PersonDTO) response.getBody();

        assertEquals(person, listPerson.get(0));
    }

    @Test
    public void shouldFindById() {
        when(mockedPersonFacade.getPersonById(any())).thenReturn(listPerson.get(0));

        ResponseEntity response = personController.findById(1);
        PersonDTO person = (PersonDTO) response.getBody();

        assertEquals(person, listPerson.get(0));
    }

    @Test
    public void shouldSavePerson() {
        when(mockedPersonFacade.savePerson(any())).thenReturn(listPerson.get(0));

        ResponseEntity response = personController.savePerson(new PersonDTO());
        PersonDTO person = (PersonDTO) response.getBody();

        assertEquals(person, listPerson.get(0));
    }

    @Test
    public void shouldUpdateAddress() {
        when(mockedPersonFacade.updatePersonAddress(any(), anyInt())).thenReturn(listPerson.get(0));

        ResponseEntity response = personController.updateAdress(new PersonUpdateAddressDTO(), 1);
        PersonDTO person = (PersonDTO) response.getBody();

        assertEquals(person, listPerson.get(0));
    }

    @Test
    public void shouldLinkPersonToPet() {
        ResponseEntity response = personController.linkPersonToPet(1, 1);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

}
