package com.cletogadelha.rbaweb.controller;

import com.cletogadelha.rbaweb.dto.PersonDTO;
import com.cletogadelha.rbaweb.dto.PersonUpdateAddressDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles({"default", "memory"})
@Transactional
public class PersonControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private PersonDTO person;

    @Before
    public void setUp() throws ParseException {
        person = createPerson();
    }

    public PersonDTO createPerson() throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        PersonDTO person = new PersonDTO();
        person.setFirstName("Cleto");
        person.setLastName("Oliveira");
        person.setAddress("Address");
        person.setDateOfBirth(format.parse("10/10/1990"));

        return person;
    }

    @Test
    public void shouldCreatePerson() throws Exception {
        mvc.perform(post("/persons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(person)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Cleto"))
                .andExpect(jsonPath("$.lastName").value("Oliveira"))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    public void shouldFindPersonById() throws Exception {
        MvcResult postResult = mvc.perform(post("/persons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(person)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andReturn();

        PersonDTO personCreated = objectMapper.readValue(postResult.getResponse().getContentAsString(), PersonDTO.class);

        mvc.perform(get("/persons/" + personCreated.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(personCreated.getFirstName()))
                .andExpect(jsonPath("$.id").value(personCreated.getId()));
    }

    @Test
    public void shouldFindPersonByName() throws Exception {
        MvcResult postResult = mvc.perform(post("/persons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(person)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.address").value("Address"))
                .andReturn();

        PersonDTO personCreated = objectMapper.readValue(postResult.getResponse().getContentAsString(), PersonDTO.class);

        mvc.perform(get("/persons?firstName=" + personCreated.getFirstName())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value(personCreated.getFirstName()))
                .andExpect(jsonPath("$.id").value(personCreated.getId()));
    }

    @Test
    public void shouldConflictWhenPersonWithSameNameCreated() throws Exception {
        mvc.perform(post("/persons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(person)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Cleto"))
                .andExpect(jsonPath("$.lastName").value("Oliveira"))
                .andExpect(jsonPath("$.id").exists());

        mvc.perform(post("/persons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(person)))
                .andExpect(status().isInternalServerError());
    }

    @Test
    public void shouldFailWhenMissingRequiredProperties() throws Exception {
        person.setAddress(null);

        mvc.perform(post("/persons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(person)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldFailWhenNoPersonFound() throws Exception {
        mvc.perform(get("/persons/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldFailWhenNoAuthentication() throws Exception {
        mvc.perform(patch("/persons/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new PersonUpdateAddressDTO())))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "user", password = "user", roles = "USER")
    public void shouldFailWhenNoAuthorization() throws Exception {
        mvc.perform(patch("/persons/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new PersonUpdateAddressDTO())))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    public void shouldUpdateUserAddress() throws Exception {
        MvcResult postResult = mvc.perform(post("/persons")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(person)))
                                .andExpect(status().isOk())
                                .andExpect(jsonPath("$.id").exists())
                                .andExpect(jsonPath("$.address").value("Address"))
                                .andReturn();

        PersonDTO personCreated = objectMapper.readValue(postResult.getResponse().getContentAsString(), PersonDTO.class);

        PersonUpdateAddressDTO personUpdateAddressDTO = new PersonUpdateAddressDTO();
        personUpdateAddressDTO.setAddress("new address");

        mvc.perform(patch("/persons/" + personCreated.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(personUpdateAddressDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("Cleto"))
                .andExpect(jsonPath("$.address").value("new address"))
                .andExpect(jsonPath("$.id").exists());
    }


}
