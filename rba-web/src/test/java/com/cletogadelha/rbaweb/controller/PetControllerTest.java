package com.cletogadelha.rbaweb.controller;

import com.cletogadelha.rbaweb.dto.PersonDTO;
import com.cletogadelha.rbaweb.dto.PetDTO;
import com.cletogadelha.rbaweb.facade.PetFacade;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PetControllerTest {

    @Mock
    private PetFacade mockedPetFacade;
    @InjectMocks
    private PetController petController;

    private List<PetDTO> listPet;

    @Before
    public void setup() {
        listPet = new ArrayList<>();
        listPet.add(new PetDTO(1, "Cleto", Integer.valueOf(4), new PersonDTO()));
        listPet.add(new PetDTO(2, "Cleto2", Integer.valueOf(6), new PersonDTO()));
    }

    @Test
    public void shouldReturnAllPets() {
        when(mockedPetFacade.getAllPets()).thenReturn(listPet);

        ResponseEntity response = petController.getAllPets();
        List<PetDTO> pets = (List<PetDTO>) response.getBody();

        assertEquals(pets, listPet);
    }

    @Test
    public void shouldSavePet() {
        when(mockedPetFacade.savePet(any())).thenReturn(listPet.get(0));

        ResponseEntity response = petController.savePet(new PetDTO());
        PetDTO pet = (PetDTO) response.getBody();

        assertEquals(pet, listPet.get(0));
    }

    @Test
    public void shouldUpdatePet() {
        when(mockedPetFacade.updatePet(any())).thenReturn(listPet.get(0));

        ResponseEntity response = petController.updatePet(1, new PetDTO());
        PetDTO pet = (PetDTO) response.getBody();

        assertEquals(pet, listPet.get(0));
    }

    @Test
    public void shouldDeletePet() {
        ResponseEntity response = petController.delete(1);

        assertNotNull(response);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

}
