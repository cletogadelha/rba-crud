package com.cletogadelha.rbarepository.specification;

import com.cletogadelha.rbarepository.model.Person;
import org.springframework.data.jpa.domain.Specification;

public class PersonSpecification {

    public static Specification<Person> byColumnNameAndValue(String columnName, String value) {
        return (root, query, builder) -> builder.equal(root.<String>get(columnName), value);
    }

}
