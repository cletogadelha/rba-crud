# Person/Pet API  

Initial implementation of an Person/Pet API using Spring Boot 2.x

## API Endpoints

* `POST` `persons` creates and returns a person
* `GET` `persons` returns all persons
* `GET` `persons/{id}` returns person by id
* `GET` `persons?firstName={name}&lastName={lastName}` returns person with name and/or lastname
* `PATCH` `persons/{id}` partially update person identified by id ( PROTECTED ENDPOINT - REQUIRES BASIC AUTH)
* `PUT` `persons/{id}/pets/{petId}` link a pet to a person


* `POST` `pets` creates and returns a pet
* `GET` `pets` returns all pets
* `PUT` `pets/{id}` updates a pet
* `DELETE` `pets/{id}` deletes a pey ( EXPERIMENTAL ENDPOINT )

# Basic Auth
User: admin, password: admin

# Experimental Endpoints
To enable experimental endpoints, change `enable.experimental.endpoints` to true;

# Profiles
* `memory` Uses h2 in-memory database (no data persisted)
* `docker-db` Uses postgres database that can be created with the provided docker-compose.yml